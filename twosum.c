#include <stdio.h>
#include <stdlib.h>
#include "hashmap.h"

size_t *twosum(int *arr, const size_t arr_length, int target) {
  // Declare a hashmap
  hashmap *mp = initHashmap();
  // Iterate through array while inserting
  for (size_t i = 0; i < arr_length; i++) {
    int remainder = target - arr[i];
    int* data = (int*) hashmapGet(mp, &remainder, sizeof(remainder));
    if (data != NULL) {
      size_t *result = (size_t*) malloc(2*sizeof(size_t));
      result[0] = i;
      result[1] = *data;
      freeHashmap(mp);
      return result;
    } else {
      int myhash = hash(mp, &arr[i], sizeof(int));
      printf("Hashing %d as %u @%zu...\n", arr[i], myhash, getHashIndex(myhash));

      myhash = hash(mp, &i, sizeof(size_t));
      printf("Hashing %zu as %u @%zu...\n", i, myhash, getHashIndex(myhash));

      hashmapPut(mp, &arr[i], sizeof(int), &i, sizeof(size_t));
    }
  }
  return NULL;
}

int main() {
  const size_t size = 10;
  int arr[size] = {501, 257, 100023, 2928, 44, 12, 10292342, 88, 123214, 0};
  int target = 12;
  size_t *result = twosum(arr, size, target);
  printf("results arr[%zu] = %d and", result[0], arr[result[0]]);
  printf(" arr[%zu] = %d\n", result[1], arr[result[1]]);
  printf(" %d + %d = %d\n", arr[result[0]], arr[result[1]], arr[result[0]] + arr[result[1]]);

  return 0;
}
