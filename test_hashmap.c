#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include "hashmap.h"

#define SIZE 128

void log_data(hashmap *mp, int key, int data, size_t index) {
    printf("Storing data: Key = %d, Data = %d, Index = %zu\n", key, data, index);
    if (mp->buckets[index] != NULL) {
        printf("Collision detected at Index = %zu\n", index);
    }
}

void check_memory(hashmap *mp) {
    // This is a simple check and by no means a replacement for specialized tools
    for (int i = 0; i < SIZE; ++i) {
        if (mp->buckets[i] != NULL) {
            mnode *node = mp->buckets[i];
            while (node != NULL) {
                if (node->key == NULL || node->data == NULL) {
                    printf("Memory leak detected!\n");
                    return;
                }
                node = node->next;
            }
        }
    }
    printf("No memory leak detected.\n");
}

void test_collision(hashmap *mp) {
    printf("\n== Testing Collisions ==\n");

    // Make dataset large enough to guarantee collisions
    for (int i = 1; i <= 500; ++i) {
        int key = i;
        int data = i * 10;
        unsigned int hashed = hash(mp, &key, sizeof(int));
        size_t index = getHashIndex(hashed);
        log_data(mp, key, data, index);
        hashmapPut(mp, &key, sizeof(int), &data, sizeof(int));
        check_memory(mp);
    }
}

void test_overwrite(hashmap *mp) {
    printf("\n== Testing Overwrites ==\n");

    int key = 5;
    int data = 9999;

    unsigned int hashed = hash(mp, &key, sizeof(int));
    size_t index = getHashIndex(hashed);

    log_data(mp, key, data, index);
    hashmapPut(mp, &key, sizeof(int), &data, sizeof(int));

    int *result = (int *) hashmapGet(mp, &key, sizeof(int));
    assert(*result == data);
    printf("Overwritten data verified. Key = %d, Data = %d\n", key, *result);
    check_memory(mp);
}

void test_retrieve(hashmap *mp) {
    printf("\n== Testing Data Retrieval ==\n");

    for (int i = 1; i <= 10; ++i) {
        int *result = (int *) hashmapGet(mp, &i, sizeof(int));
        if (result != NULL) {
            printf("Retrieved: Key = %d, Data = %d\n", i, *result);
        } else {
            printf("Data for Key = %d not found.\n", i);
        }
    }
    check_memory(mp);
}

int main() {
    printf("== Hashmap Bruteforce Testing ==\n");
    hashmap *mp = initHashmap();

    test_collision(mp);
    test_overwrite(mp);
    test_retrieve(mp);

    printf("\n== Freeing Hashmap ==\n");
    freeHashmap(mp);

    printf("\n== Tests Completed Successfully ==\n");

    return 0;
}

