#include "hashmap.h"
#include <string.h>
#include <time.h>

// Cleaner dereference when working with void pointers
#define DEREF_AS(type, var) (*(type*) var)
#define getNode() (mp->buckets[index])
#define SIZE 128

unsigned int hash(hashmap *mp, void *key, size_t key_size) {
  return DEREF_AS(unsigned int, key)*mp->seed;
}

size_t getHashIndex(unsigned int hash) {
  return hash & (SIZE - 1);
}

hashmap *initHashmap() {
  hashmap *mp = (hashmap*) malloc(sizeof(hashmap));
  // Initialize SIZE buckets with NULL
  mp->buckets = (mnode**) calloc(SIZE, sizeof(mnode*));
  srand(time(0));
  // Hashing entropy
  mp->seed = (unsigned int) rand();
  return mp;
}

void freeHashmap(hashmap *mp) {
  for (int i = 0; i < SIZE; i++)
    if (mp->buckets[i] != NULL) {
      mnode *curr = mp->buckets[i];
      mnode *tmp;
      while (curr != NULL) {
        tmp = curr;
        curr = curr->next;
        free(tmp->key);
        free(tmp->data);
        free(tmp);
      }
    }
  free(mp->buckets);
  free(mp);
}

// String feature?
void hashmapPut(hashmap *mp, void* key, size_t key_size, void* data, size_t data_size) {
  // Hash the key and get index, if there is a collision
  // in the bucket, link list to the next node

  // Hash and index
  size_t index = getHashIndex(hash(mp, key, key_size));
  // Check for collision
  if (getNode() == NULL) {
    // Build node with cloned data
    mnode *nd = (mnode*) malloc(sizeof(mnode));
    // Clone key
    nd->key = (void*) malloc(key_size);
    memcpy(nd->key, key, key_size);
    nd->key_size = key_size;
    // Clone data
    nd->data = (void*) malloc(data_size);
    memcpy(nd->data, data, data_size);
    nd->data_size = data_size;
    // Set next as null
    nd->next = NULL;

    getNode() = nd;
    
    // Keys are equal, update data
  } else if (!memcmp(getNode()->key, key, key_size)) {
    free(getNode()->data);
    getNode()->data = (void*) malloc(data_size);
    memcpy(getNode()->data, data, data_size);
    getNode()->data_size = data_size;

    // Collision, link list next item
  } else {
    mnode *ptr;
    for (ptr = getNode(); ptr->next != NULL; ptr = ptr->next);

    // Build node with cloned data
    mnode *nd = (mnode*) malloc(sizeof(mnode));
    // Clone key
    nd->key = (void*) malloc(key_size);
    memcpy(nd->key, key, key_size);
    nd->key_size = key_size;
    // Clone data
    nd->data = (void*) malloc(data_size);
    memcpy(nd->data, data, data_size);
    nd->data_size = data_size;
    // Set next as null
    nd->next = NULL;

    ptr->next = nd;
  }
}

void *hashmapGet(hashmap *mp, void* key, size_t key_size) {
  // Get hash and access buckets while also checking if
  // the node in question is what we are looking for,
  // confirm by checking the key against the node's key.
  
  size_t index = getHashIndex(hash(mp, key, key_size));
  mnode *ptr = getNode();
  // Deprecated single byte comparison
  // while (getNode() != NULL && DEREF_AS(int, getNode()->key) != DEREF_AS(int, key)) {
  while (ptr != NULL && memcmp(ptr->key, key, key_size)) {
    ptr = ptr->next; 
  }
  if (ptr == NULL) return NULL;
  else return ptr->data;
}
