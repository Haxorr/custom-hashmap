#include <stdlib.h>

#ifndef HASHMAP_H
#define HASHMAP_H

// hashmap(key, data) both void for any use

typedef struct mnode {
  void *key;
  size_t key_size;
  void *data;
  size_t data_size;
  struct mnode *next;
} mnode;

typedef struct {
  mnode **buckets;
  unsigned int seed;
} hashmap;

unsigned int hash(hashmap * mp, void *key, size_t key_size);

size_t getHashIndex(unsigned int hash);

hashmap *initHashmap();

void freeHashmap(hashmap *mp);

void hashmapPut(hashmap *mp, void* key, size_t key_size, void* data, size_t data_size);

void *hashmapGet(hashmap *mp, void* key, size_t key_size);

void hashmapPop(hashmap *mp, void *key, size_t key_size);

void *getMax(hashmap *mp);

#endif
