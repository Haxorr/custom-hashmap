# Compiler settings
CC = gcc
CFLAGS = -Wall

# Source files and executable names
HASHMAP_SOURCES = test_hashmap.c hashmap.c
TWOSUM_SOURCES = twosum.c hashmap.c
HASHMAP_EXECUTABLE = test_hashmap
TWOSUM_EXECUTABLE = twosum_test

all: test_hashmap twosum

test_hashmap: 
	$(CC) $(CFLAGS) $(HASHMAP_SOURCES) -o $(HASHMAP_EXECUTABLE)

twosum:
	$(CC) $(CFLAGS) $(TWOSUM_SOURCES) -o $(TWOSUM_EXECUTABLE)

run_test_hashmap: test_hashmap
	./$(HASHMAP_EXECUTABLE)

run_twosum: twosum
	./$(TWOSUM_EXECUTABLE)

clean:
	rm -f $(HASHMAP_EXECUTABLE) $(TWOSUM_EXECUTABLE)

